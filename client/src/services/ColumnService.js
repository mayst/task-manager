import Api from './Api'

export default {
  getAll () {
    return Api().get('columns')
  },
  add (column) {
    return Api().post('columns', column)
  },
  move (id, to) {
    return Api().put(`columns/${id}`, to)
  },
  delete (id) {
    return Api().delete(`columns/${id}`)
  }
}
