import Api from './Api'

export default {
    add (id, title) {
        return Api().post(`columns/${id}/cards`, title)
    },
    move (columnId, params) {
        return Api().put(`columns/${columnId}/cards`, params)
    },
    removeFromColumn (columnId, cardId) {
        return Api().delete(`columns/${columnId}/cards/${cardId}`)
    },
    moveInsideColumn (columnId, cardId, params) {
        return Api().put(`columns/${columnId}/cards/${cardId}`, params)
    }
}