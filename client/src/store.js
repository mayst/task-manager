import Vue from 'vue'
import Vuex from 'vuex'

import ColumnService from '@/services/ColumnService'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    title: 'My task manager',
    columns: [],
  },
  getters: {
    columns: state => state.columns
  },
  mutations: {
    SET_COLUMNS (state, columns) {
      if (!columns) { return }
      state.columns = columns
    },
    ADD_COLUMN (state, column) {
      state.columns.push(column)
    },
    DELETE_COLUMN (state, index) {
      state.columns.splice(index, 1)
    },
    SET_CARDS (state, data) {
      state.columns.find(el => {
        if (el._id === data.id) {
          el.cards = data.cards
        }
      })
    },
    ADD_CARD (state, data) {
      state.columns.find(el => {
        if (el._id === data.columnId) {
          el.cards.push(data.card)
        }
      })
    }
  },
  actions: {
    async loadDesk ({ commit }) {
      try {
        const response = await ColumnService.getAll()
        commit('SET_COLUMNS', response.data.columns)
        console.log(response.data.columns)
      } catch (e) {
        console.log(e)
      }
    }
  }
})
