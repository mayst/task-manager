const mongoose = require('mongoose')
const Card = require('./Card')

const Column = new mongoose.Schema({
    title: {
        type: String,
        required: [ true, 'Need a title' ],
        min: [ 1, 'Too short title' ],
        max: [ 50, 'Too long title' ]
    },
    cards: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Card'
    }],
    created_at: {
        type: Date
    },
    updated_at: {
        type: Date
    }
})

module.exports = mongoose.model('Column', Column)