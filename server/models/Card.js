const mongoose = require('mongoose')

const Card = mongoose.Schema({
    title: {
        type: String,
        required: [ true, 'Need a title' ],
        min: 1,
        max: 50
    },
    description: {
        type: String,
        min: 1,
        max: 200
    },
    created_at: {
        type: Date
    },
    updated_at: {
        type: Date
    }
})

module.exports = mongoose.model('Card', Card)