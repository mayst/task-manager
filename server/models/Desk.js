const mongoose = require('mongoose')
const Column = require('./Column')

const Desk = new mongoose.Schema({
    title: {
        type: String,
        min: 1,
        max: 50
    },
    columns: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Column'
    }],
    created_at: {
        type: Date
    },
    updated_at: {
        type: Date
    }
})

module.exports = mongoose.model('Desk', Desk)