const Column = require('./../models/Column')
const Card = require('./../models/Card')
const mongoose = require('mongoose')

const create = async (req, res) => {
    const columnId = req.params.id
    const title = req.body.title

    if (!columnId && !title) { return res.status(404).send('can`t get needed params') }

    try {
        const newCard = new Card({
            title: title,
            created_at: new Date()
        })

        let [ dbResp, column ] = await Promise.all([ newCard.save(), Column.findById(columnId) ])
        await column.cards.push(dbResp)
        await column.save()

        res.send(dbResp)
    } catch (e) {
        res.status(404).send(e)
    }
}
module.exports.create = create

const move = async (req, res) => {
    const columnId = req.params.id
    const cardId = req.body.cardId
    const index = req.body.index

    if (!columnId || !cardId || index === undefined) { return res.status(404).send('can`t get needed params') }

    try {
        const column = await Column.findById(columnId)
        await column.cards.splice(index, 0, mongoose.Types.ObjectId(cardId))
        column.updated_at = new Date()
        await column.save()

        res.send('card has been moved')
    } catch (e) {
        res.status(404).send(e)
    }
}
module.exports.move = move

const moveInsideColumn = async (req, res) => {
    const columnId = req.params.id
    const cardId = req.params.cardId
    let from = req.body.from
    let to = req.body.to

    if (!columnId || !cardId) { return res.status(404).send('can`t get needed params') }

    if (from < to) {
        to++
    } else {
        from++
    }

    try {
        const column = await Column.findById(columnId)
        await column.cards.splice(to, 0, mongoose.Types.ObjectId(cardId))
        await column.cards.splice(from, 1)
        column.updated_at = new Date()
        await column.save()

        res.send(column)
    } catch (e) {
        res.status(404).send(e)
    }
}
module.exports.moveInsideColumn = moveInsideColumn

const removeFromColumn = async (req, res) => {
    const columnId = req.params.id
    const cardId = req.params.cardId

    if (!columnId || !cardId) { return res.status(404).send('can`t get nedeed params') }

    try {
        const column = await Column.findById(columnId)
        await column.cards.remove(mongoose.Types.ObjectId(cardId))
        column.updated_at = new Date()
        await column.save()

        res.send('card has been removed')
    } catch (e) {
        res.status(404).send(e)
    }
}
module.exports.removeFromColumn = removeFromColumn
