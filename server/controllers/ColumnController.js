const Column = require('./../models/Column')
const Desk = require('./../models/Desk')
const mongoose = require('mongoose')

const create = async (req, res) => {
    const column = req.body.column

    try {
        const newColumn = await new Column({
            title: column.title,
            created_at: new Date()
        })
        newColumn.save()

        let desk = await Desk.findOne({ title: 'my-first-desk' })

        if (!desk) {
            const newDesk = new Desk({ title: 'my-first-desk', created_at: new Date() })
            desk = await newDesk.save()
        }

        await desk.columns.push(newColumn)
        await desk.save()

        res.send(newColumn)
    } catch (e) {
        res.status(500).send(e)
    }
}
module.exports.create = create

const getAll = async (req, res) => {
    try {
        const desk = await Desk
                            .findOne({ title: 'my-first-desk' })
                            .populate({
                                path: 'columns',
                                populate: {
                                    path: 'cards',
                                }
                            })
                            .exec()
        res.send(desk)
    } catch (e) {
        res.status(404).send(e)
    }
}
module.exports.getAll = getAll

const remove = async (req, res) => {
    const id = req.params.id

    if (!id) { return res.status(404).send('can`t get id') }

    try {
        const desk = await Desk.findOne({ title: 'my-first-desk' })
        await desk.columns.remove(mongoose.Types.ObjectId(id))
        await desk.save()
        await Column.findByIdAndDelete(id)

        res.status(200).send('column successfully deleted')
    } catch (e) {
        res.status(404).send(e)
    }
}
module.exports.remove = remove

const move = async (req, res) => {
    const id = req.params.id
    let from = req.body.from
    let to = req.body.to

    if (!id || from === undefined || to === undefined) { return res.status(404).send('can`t get needed params') }

    if (from < to) {
        to++
    } else {
        from++
    }

    try {
        const desk = await Desk.findOne({ title: 'my-first-desk' })
        await desk.columns.splice(to, 0, mongoose.Types.ObjectId(id))
        await desk.columns.splice(from, 1)
        desk.updated_at = new Date()
        await desk.save()

        res.send(desk.columns)
    } catch (e) {
        res.status(404).send(e)
    }

}
module.exports.move = move
