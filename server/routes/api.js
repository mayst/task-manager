const express = require('express')
const router = express.Router()

const ColumnController = require('./../controllers/ColumnController')
const CardController = require('./../controllers/CardController')

router.get('/columns', ColumnController.getAll)

router.post('/columns', ColumnController.create)

router.put('/columns/:id', ColumnController.move)

router.delete('/columns/:id', ColumnController.remove)


router.post('/columns/:id/cards', CardController.create)

router.put('/columns/:id/cards', CardController.move)

router.put('/columns/:id/cards/:cardId', CardController.moveInsideColumn)

router.delete('/columns/:id/cards/:cardId', CardController.removeFromColumn)

module.exports = router
