const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const connectToDb = require('./../db/connect')
const api = require('./../routes/api')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

connectToDb()

app.use('/', api)

app.listen(process.env.PORT || 3000)